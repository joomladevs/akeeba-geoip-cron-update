# akeeba-geoip-cron-update

This project provides an easy php script to auto update Akeeba GeoIp database using PHP.

If you don't want to deal with this you may just use the cronjob command suggested on this thread: https://www.akeebabackup.com/support/admin-tools/Ticket/30757