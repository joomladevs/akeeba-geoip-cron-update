<?php

use Joomla\Archive\Archive;

const _JEXEC = 1;

if (file_exists(dirname(__DIR__) . '/defines.php'))
{
		require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
		define('JPATH_BASE', dirname(__DIR__));
			require_once JPATH_BASE . '/includes/defines.php';
}

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Akeeba GeoIP library
require_once JPATH_PLUGINS . '/system/akgeoip/lib/akgeoip.php';
require_once JPATH_PLUGINS . '/system/akgeoip/lib/vendor/autoload.php';

// Configure error reporting to maximum for CLI output.
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Load Library language
$lang = JFactory::getLanguage();

// Try the files_joomla file in the current language (without allowing the loading of the file in the default language)
$lang->load('files_joomla.sys', JPATH_SITE, null, false, false)
// Fallback to the files_joomla file in the default language
	|| $lang->load('files_joomla.sys', JPATH_SITE, null, true);

/**
 * This script will fetch the update information from MaxData
 *
 * @since 1.0
 *
 **/
class GeoIpUpdate extends JApplicationCli
{
	/**
 	 * Entry point for CLI script
 	 */
	public function doExecute()
	{
		$akGeoIp = new AkeebaGeoipProvider;

		$result = $akGeoIp->updateDatabase();

		$this->out($result);
	}
}

JApplicationCli::getInstance('GeoIpUpdate')->execute();
